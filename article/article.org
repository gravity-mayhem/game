#+title: Article

* DONE Introducció
Un dels primers i més memorables videojocs va ser [[https://archive.org/details/pdp1_spacewar][Spacewars!]], creat el 1961 per al DEC PDP-1 amb l'objectiu de demostrar les capacitats gràfiques d'aquesta màquina amb un valor inicial de 130.000$, equivalent avuí en dia a un milió de dolars.

Spacewars tractava de dues naus espacials enfrentades en un duel de dos jugadors al voltant del camp gravitacional d'una estrella.

A primera vista un joc simplista, el fet que és impossible escapar de la forca gravitacional de l'estrella si el jugador accelera en la direcció oposada li proveeix d'un dinamisme i una profunditat inusuals.

Es pot jugar al joc a través d'un emulador a [[https://archive.org/details/pdp1_spacewar][archive.org]], gràcies als esforços d'uns voluntaris per a la seva preservació.

#+CAPTION: Spacewars!
[[./images/spacewars-resized.jpg]]


El títol del meu joc és *Gravity Mayhem*. Segons la proposta del projecte, aquest havia de consistir en un un successor espiritual de Spacewars!, jugat en xarxa. Un joc de naus espacials, en 2D, per equips o tots contra tots que hauria seguit les lleis de la física més fidelment que cap altre joc del gènere avuí en dia, aportant una nova dimensió estratégica.

Aquesta era la intenció. El projecte, però, ràpidament es va convertir en dues coses: la primera és la realització de l'esforç titànic que soposa fer un videojoc en xarxa des de zero amb una llibreria de baix nivell i sense coneixements previs de la matéria. La segona és l'estudi d'aquells patrons arquitectonics utilitzats a la indústria dels videojocs que permeten gestionar la complexitat d'un programa en el que absolutament tots els components estan interconectats i en el que la forma del projecte és contínuament maleable.

Aquest article és el resultat de la meva recerca personal de tots aquests temes. Espero que sigui interessant, i que contingui coneixements útils per a qualsevol que es vulgui endinsar al mon de la programació de videojocs, tant diferent de la programació habitual que mereix la utilització dels seus propis patrons arquitectònics.

Aquest document senta les bases per a una serie serie orientada a la iniciació del diseny i la programació de videojocs, traduit a l'anglés i al castellà, i que contindrà totes aquelles lliçons i pedres al camí que m'he anat trobant durant el transcurs del projecte.

** DONE L'estil de l'article
A l'article s'utilitza la primera persona lliurament. L'ús de la tercera persona intenta transmetre la noció que la idea essent discutida és lliure de valors, neutral i imparcial. Aquest rarament és el cas al nostre ofici, l'exercici del qual és les més de les vegades més xamànic que científic, basat com ho està en l'experiència personal.

** DONE Notació
*** DONE Diagrames UML
Tots els camps d'una classe UML són propietats. Aixó vol dir que tot camp públic representa també un getter i un setter implicits a menys que s'indiqui lo contrari. Les dues classes següents són equivalents:
#+begin_src plantuml
@startuml
hide empty members
class CarWithAProperty {
        + speed: Float
}
class CarWithAField {
        + getSpeed(): Float
        + setSpeed(speed: Float)
}
@enduml
#+end_src

#+RESULTS:
: /tmp/babel-grFfNB/plantuml-oGVRcu.png

* TODO Tecnologies
Aquésta secció està dedicada a presentar tot el programari fet servir al projecte. Cert programari, com en el cas de Git, no sol ser utilitzat a la indústria dels videojocs de gran pressupost, que presenta dificultats i necessitats molt específiques, però encara és de gran utilitat per als desenvolupadors independents.

** El llenguatge: Kotlin
Crec que el llenguatge fet servir per a programar el joc mereix una secció per a si mateix. Així de bó és.

Aquest és el llenguatge que Java hauria d'haver sigut. Creat per JetBrains, l'empresa darrere de l'IntelliJ Idea, Kotlin està inspirat per Java, Scala, C# i Groovy, entre d'altres. Algunes de les caracteristiques del llenguatge serien les següents:

- Té interoperabilitat completa amb Java.
- La documentació és superba.
- El sistema de tipus és més potent (i estringent) que el de Java en tots els sentits.
- No t'obliga a gestionar les escepcions, sinó que et dona eines per a fer el codi més segur.
- A través de la sintaxi es poder fer servir patrons OOP com l'Observer, els delegats, els POJOs i els Singletons especificant-los amb una paraula clau, i el compilador produirà el codi automàticament.
- Es poden crear llenguatges de domini específic a través de les seves funcions d'extensió amb receptor.

Per a donar una idea de les capacitats d'aquest llenguatge, posaré un dels exemples més famosos: Kotlin té un repertori d'operadors i un sistema de tipus encarats a tractar amb el valor *NULL* i evitar les NullPointerExceptions.
El que a Java seria ~if (foo != null) { doSomething() }~ a Kotlin es converteix en ~foo?.doSomething()~. Si a més volguessim oferir una acció alternativa en cas que la propietat fos =null=, escriuriem ~foo?.doSomething ?: doAnotherThing().~

Usualment però, només es tracta amb =null= quan es fà servir classes escrites en Java, perquè el sistema de tipus t'obliga a especificar sempre l'us de =null=. A Kotlin cada tipus (per exemple =String=) té la seva contrapartida nulificable (p. ex. =String i String?). Així l'ús del valor =null= així com la seva gestió serien sempre explícits.

Kotlin es pot compilar de forma nativa (sense la JVM) i transpilar a JavaScript.

** TODO Biblioteques

*** LibGDX
Al cor del projecte està LibGDX, una llibreria de baix nivell que permet fer jocs multiplataforma. No obliga a fer servir cap arquitectura en específic, però ofereix classes per a fer més senzilla la utilització de certs patrons. Així per exemple, proveeix al programa d'una intància d'OpenGL, la API multiplataforma de renderització més utilitzada a la indústria (la API més utilitzada és DirectX, però no és multiplataforma). LibGDX gestiona aquesta instància automàticament i proveeix de métodes i classes útils per a treballar-hi, però també hi ofereix accés directe si un es vol embrutar les mans.

*** LibKTX
És una serie de diverses extensions modulars de les APIs de LibGDX escrites en Kotlin. Permet fer servir la llibreria de forma més idiomàtica; ofereix constructors i accessors en forma de DSL i extén els tipus existents a LibGDX.

*** TODO Box2D
Un motor de físiques s'encarrega de calcular les colisions, una de les parts més difícils de programar de qualsevol joc, i la forma en la que interactuen els objectes físics entre ells quan colisionen.

Box2D fà servir unitats del sistema internacional, usualment referides com a "MKS" (metre-kilogram-second).

LibGDX proveeix un wrapper de la API de Box2D, que està escrit en C++. LibKTX al seu torn facilita la instanciació de cossos rígids.

**** Determinisme a Box2D
[[https://box2d.org/documentation/md__d_1__git_hub_box2d_docs__f_a_q.html#autotoc_md155][Documentació pertinent]]

El [[https://en.wikipedia.org/wiki/Determinism][determinisme]] és una postura filosòfica segons la qual el desenvolupament de tots els events ve determinat per causes previes.

Definim un sistema determinista (no determinístic) com a aquell que sempre produeix el mateix resultat donat el mateix argument o dades d'entrada.

Una simulació Box2D és deterministica llevat del mateix binari a la mateixa plataforma, i això ho comparteix amb una gran majoria de simulacions en temps real. Aquesta estringència es deguda a l'estructura i gestiò dels números de coma flotant ([[https://es.wikipedia.org/wiki/IEEE_754][estandard IEEE 754]]), que priorititzen els resultats de la major practicalitat possible per a un ús científic [yosefk], a consta de qualsevol altre propietat, i que fan que els resultats de les operacions realitzades amb aquest números variin depenent de diversos factors, entre d'altres:
- Optimitzacions algebraiques del compilador, que varien de versió en versió.
- L'us d'instruccions complexes com "sinus" o "[[https://en.wikipedia.org/wiki/Multiply%E2%80%93accumulate_operation][multiplicar-accumular]]"
- Problemes inherents de la forma en la que l'arquitectura x86 realitza els calculs.

**** Box2D en jocs multijugador
Aquí presento un sil·logisme: Box2D és determinista llevat del mateix binari executat a la mateixa plataforma. Un joc multijugador necessita una simulació per a cada un dels jugadors, i també necessita que les simulacions estiguin sincronitzades en tot moment. Per tant un joc multijugador, que s'executa en més d'una plataforma, no pot comptar amb el determinisme de les simulacions per a sincronitzar l'estat dels jugadors.

La sincronització d'una simulació requereix de coneixements de programació en xarxa (com la creació d'un protocol específic utilitzant datagrams) i d'altres técniques que estan fora de l'avast d'aquest treball, però que exposaré en seccions posteriors.

**** TODO Anatomia d'un cos
**** TODO Anatomia d'una col·lisió
**** TODO Anatomia d'un món (classe World)
**** TODO El debugger
** Frameworks
*** Ashley
Ashley és el cor de l'arquitectura ECS (Entitat-Component-Sistema) del projecte.
Orientada a fer jocs senzills i interfícies d'usuari, he comprobat massa tard que aquest framework minimalista es queda curt per a les necessitats del projecte. Tot i això, ha sigut perfecte per a aprendre d'aquesta arquitectura orientada a les dades, tant diferent de la programació orientada a objectes tradicional. Més informació sobre aquesta arquitectura a la secció pertinent.

Amb Ashley, fer jocs senzills és encara més fàcil, mentre que fer jocs complexos es torna un exercici de paciencia mentre un reinventa la roda.

** Control de versions amb Git
L'estrella de la funció. Dubto que necessiti de cap presentació; tota la indústria de la programació el fà servir. Tota excepte la dels videojocs.

El gran problema de Git en el desenvolupament de videojocs és que no gestiona eficientment els arxius binaris. Hi ha hagut solucions, com [[https://git-lfs.github.com/][GitLFS (Git Large File Storage)]], que en comptes de guardar l'arxiu binari guarda un punter cap a un servidor extern, amb tota la infraestructura extra que això comporta.

Per a donar una idea del que soposaria fer servir Git en un joc amb grans arxius binaris: Git guarda la informació dels arxius en objectes anomenats "blobs", que son els mateixos arxius amb un hash SHA-1 associat que farà servir per adressar-s'hi a partir del moment del commit. Més endevant Git comprimeix aquesta informació i la transforma en deltas. Això permet al sistema ser eficient tant en temps com en espai.

Ara imaginem una imàtge. Com que no és un arxiu de text sinó un binari, no se'n pot extreure cap delta. Si volguessim canviar les metadades, git guardaria una còpia exacta de la imatge, que quedaria per a la posteritat a l'historial. Ara fem el mateix amb les imatges 3D, els vídeos, etc. Podem imaginar la quantitat absurda d'espai d'emmagatzematge que ocuparia un repositori de Git d'un joc una mica complex.

Deixant de banda el problema dels arxius binaris, també s'ha de tenir en compte que Git no és un sistema fàcil de fer servir per a persones amb un rerefons prou tècnic. No és intuitiu. I tot estudi dedicat a la creació de videojocs té el seu apartat d'escriptors, l'apartat d'art, i altres persones que també han de tenir accés al repositori.

Dit tot això, per les característiques del projecte no he considerat necessari l'us d'un sistema de control de versions especialitzat. Git m'ha servit esplendidament.

*** Magit: El client de Git definitiu
Hi ha programadors per als que treure les mans del teclat és un pecat, programadors per als que l'ús del ratolí es una penitència imposada pels temps moderns. Programadors que incrusten Vim i Emacs a dins el seu IDE preferit i que si no poden fer servir la terminal per a tot comencen a tenir tics nerviosos. Jo soc un d'aquests programadors, i fins fa poc feia servir Git des de la terminal. Però ja no, perquè he trobat la eina definitiva: Magit.

#+CAPTION: Magit
[[./images/magit-resized.png]]

** Compilació i gestió de dependències amb Gradle
Un sistema de muntatge de projectes i de gestió de dependències molt general amb el que, a través de la seva API (poc intuitiva però molt potent) es pot crear extensions per a qualsevol llenguatge i tasca.

És compatible amb repositoris de Maven i segueix els seus mateixos estandards (com l'estructura de directoris). La gestió de subprojectes es torna senzilla. Aquestes i altres raons fan que els desenvolupadors de LibGDX l'hagin escollit com al seu gestor per defecte.

** Altres eines
*** GDX Liftoff
El creador de projectes oficial de LibGDX és molt útil i fàcil de fer servir, però presenta poques opcions. GDX Liftoff ofereix opcions per a crear el projecte amb llibreries no oficials, i conté una selecció molt major de plantilles.

*** GDX Texture Packer
Els jocs en 2D tenen dues opcions a l'hora de presentar imàtges: la primera és fer servir sprites, el terme utilitzat per a imatges que seran renderitzades a dins el joc. Aquestes imatges ja existiran a l'hora d'executar el joc. La segona és renderitzar models en 3D en el mon de dues dimensions. Això es coneix com a faux 3D o com a 2.5D.

En aquest projecte es fà servir la primera metodologia. GDX Texture Packer permet comprimir moltes imatges en una de sola i crear un punter amb les direccions i les mides de cada una. Així la GPU no ha de canviar la imatge en memòria, i renderitzar les imatges es torna molt més eficient.

A més de la interfície gràfica, també es presenta una API per a comprimir les imatges contingudes a una carpeta de forma automàtica, que es pot fer servir amb Gradle.

* TODO Patrons presents al projecte
En aquesta secció es presenten alguns dels patrons més importants del projecte, aquells que dicten la dependència entre els components o que imposen una forma de programar diferent.

Donat que alguns d'aquests patrons poden prendre una forma diferent cada cop que s'apliquen (sobretot l'ECS, que està vagament definit), la presentació de cada un serà breu.

** TODO Patrons de sequenciació
*** TODO Loop del joc
Potser el patrò més important del joc, es tracta d'actualitzar els components seqüencialment en passos de temps discrets. LibGDX facilita i encoratja la seva utilització, i proveeix del temps perquè el programador no s'hagi de preocupar de la seva adquisició.

La seva pot ser una forma tant senzilla com aquesta:
#+begin_src c++
double t = 0.0;
double dt = 1.0 / 60.0;

while ( !quit )
{
    integrate( state, t, dt );
    render( state );
    t += dt;
}
#+end_src
A la funció anterior, ~integrate~ també es podria anomenar ~simulate~, perquè la seva funció és actualitzar l'estat del joc en passos de temps ~dt~ discrets.

O pot ser una forma tant complexa com aquesta:
#+begin_src c++
double t = 0.0;
double dt = 0.01;

double currentTime = hires_time_in_seconds();
double accumulator = 0.0;

State previous;
State current;

while ( !quit )
{
    double newTime = time();
    double frameTime = newTime - currentTime;
    if ( frameTime > 0.25 )
        frameTime = 0.25;
    currentTime = newTime;

    accumulator += frameTime;

    while ( accumulator >= dt )
    {
        previousState = currentState;
        integrate( currentState, t, dt );
        t += dt;
        accumulator -= dt;
    }

    const double alpha = accumulator / dt;

    State state = currentState * alpha +
        previousState * ( 1.0 - alpha );

    render( state );
}
#+end_src
El codi anterior desacopla el temps de renderitzat del temps de simulació. Intuitivament podem dir que el renderitzat genera temps en passos de temps de quantitat variable (usualment aleatoria), i la simulació el consumeix en passos de temps de quantitat discreta.

** TODO Patrons arquitectònics
*** TODO Patró Entitat-Component-Sistema
L'ECS és un patró orientat a les dades utilitzat pels motors Unity i Unreal Engine i per una quantitat cada cop major de productors de videojocs amb motors independents, perqué encara que precisa de l'aprenentatge d'un nou paradigme, els beneficis son evidents en la quantitat inmensa de nou contingut que produeixen aquestes empreses per als seus jocs, que s'actualitzen amb nous personatges, nous aspectes visuals, nous nivells i solucions a errors cada setmana.

El patró està vàgament definit, i cada implementació és única, ja que no defineix una estructura fixa (com seria el cas d'un patró OOP tant conegut com el factory) sinó un conjunt de termes i principis. Algunes de les seves característiques més distintives son que rebutja l'herència d'objectes a favor de la composició en la seva forma més extrema, i que un sistema ECS es pot modelar com a una base de dades relacional.

**** Terminologia
El patró ECS defineix tres termes clau:
- Les *entitats*, que són poc més que un contenidor per a Components. Poden representar una gran varietat d'objectes.
- Els *components*, que son objectes que només contenen estat (com podria ser posició, rotació o una imàtge)
- Els *sistemes*, que proveeixen de comportament al conjunt i iteren seqüencialment sobre els components.

Les entitats necessiten un mètode d'identificació únic i una forma de relacionar components entre ells. Això pot ser una referència o un ID.

Els components poden contenir qualsevol mena d'informació. Poden ser simples marcadors --a Gràvity Mayhem hi ha un marcador anomenat ~CameraFocus~, i qualsevol entitat que el tingui serà seguida per la càmera--, o poden contenir la informació necessaria per al funcionament d'un sistema, com la simulació física. Aquest últim és un patró dins ĺ'ECS, és conegut com a Singleton Component i conté tot l'estat que hauria existit dins d'un sistema.

Per últim, cada sistema itera sobre tots els components d'un o diversos tipus i actua sobre les seves dades.

Així, els components són les dades del joc i els sistemes són subrutines que actuen sobre aquestes dades de forma seqüencial.

**** La forma del patró
En la seva forma més simple, una *entitat* està composada per *components*, i cada sistema iterarà en ordre les entitats que continguin els components que se li indiquin.
[[./images/ECS-shape-resized.png]]

Per exemple, el sistema que simula les interaccions físiques iteraria sobre totes les entitats que continguin el component "RigidBody", i les actualitzaria.
[[./images/ECS-process-resized.png]]

**** Principis
El patró defineix una serie de principis que s'ha de seguir:
1. Els sistemes no tenen estat.
2. Els components no tenen comportament.
3. Els sistemes es comuniquen entre ells a traves de les dades.
4. No hi ha comunicació directa entre els components.
5. S'ha d'evitar la interdependència entre els Sistemes.

Haber de trencar les dues primeres premises és un indicatiu d'una mala arquitectura, i sempre s'ha de refactoritzar. Trencar qualsevol altre premisa, si no és premeditat, pot comportar a una complexitat i una interdependència major, no menor, i fer que el patró es torni superflu i contraproduent. Necessita una menció especial el punt 5, ja que la dependencia entre dos sistemes és subtil i pot prendre més d'una forma. Un exemple seria la dependencia temporal, és a dir, que aplicar un sistema abans que un altre canvii el resultat final. Això fa impossible el paral·lelisme d'aquest dos sistemes en particular, i no hi ha una forma fàcil de seguir aquesta dependència sense estudiar el codi a fons o documentar-ho explicitament.

L'ECS facilita enormement l'adició de nova funcionalitat i la gestió de la complexitat d'un gran projecte. A canvi, la complexitat inicial és més elevada, es fà necessaria la planificació de l'estructura i es restringeixen les formes d'aportar codi al projecte.

**** TODO L'ECS modelat com a BD relacional

** TODO Patrons de desacoplament
**** DONE Localitzador de servei
El localitzador de servei desacopla el codi que necessita un servei tant de la implementació concreta del servei com de com localitzar la implementació.

Un exemple de localitzador de servei seria el sistema d'audio de Gravity Mayhem:
#+begin_src plantuml
@startuml
hide empty members
class AudioLocator {
        + service: Service
}

interface Audio {
        + enabled: Boolean
        + play(soundAsset: SoundAsset, volume: Float)
        + fun play(musicAsset: MusicAsset, volume: Float, loop: Boolean)
        + fun pause()
        + fun resume()
        + fun stop(clearSounds: Boolean)
        + fun update()
}

class DefaultAudio implements Audio
class NullAudio implements Audio
class DebugAudio implements Audio

AudioLocator ..> "1" Audio
@enduml
#+end_src

#+RESULTS:
[[file:/tmp/babel-23h51B/plantuml-6R5bS5.png]]

Per tal de servir l'audio al joc, una classe externa hauria d'instanciar una implementació de la classe Audio i assignar-la al localitzador. A partir d'aquell moment, tot codi que accedeixi al servei accedirà a aquella implementació en particular. A més al accedir sempre a través del localitzador, la classe que l'utilitzi mai en guardarà cap referència.

**** TODO Command?
**** DONE Cua d'events
Aquest patró seria la contrapartida asíncrona del patró observador, i serveix per a desacoplar els transmissors i els receptors d'events *en el temps*. La forma del patró dependrà del numero d'emisors i de receptors, i de com es controla com es processen els events.

A Gravity Mayhem, ha estat necessari desacoplar la transmissió dels events provinents dels contàctes físics transmesos pel motor de Box2D de la seva gestió per la resta del sistema ECS. Creant una cua d'events per a recollir i postergar el processament dels contactes ha estat clau per a una simulació sense interrupcions.

#+begin_src plantuml
@startuml
class World {
        + addContactListener(listener: ContactListener)
        + removeContactListener(listener: ContactListener)
}
interface ContactListener {
        + beginContact(contact: Contact)
        + endContact(contact: Contact)
        + preSolve(contact: Contact, oldManifold: Manifold)
        + postSolve(contact: Contact, impulse: ContactImpulse)
}
class PhysicsEventQueue {
        + dispatchEvents()
}
@enduml
#+end_src

** TODO Patrons d'optimització
*** TODO Object Pool

* TODO Arquitectura de Gravity Mayhem
Comencaré esplaiant les classes principals, recorrent el fluxe d'informació per tot el programa.
Després explicaré la funció i l'estructura de l'ECS, el sistema d'audio i els events, per a oferir una intuició sobre cada component i les seves interaccions.

Cal a dir que el joc no està acabat, i parts tant necessaries com el menú principal encara estan absents. Així i tot el joc ja és prou madur com per ser jugat, sense objectius concrets.

** DONE Les classes principals
*** DONE Lwjgl3Application
Aquesta classe correspondria a l'antiga ~DesktopLauncher~ de LibGDX, i la seva funció és configurar i executar el client d'escriptori.

Com que totes les plataformes tenen codi en comú, cada una té un punt d'entrada diferent, com ~Lwjgl3Application~ i ~AndroidLauncher~.

*** DONE Game
~Game~ és la classe principal, i conté referències a dos instàncies de Viewport, una per al joc i l'altre per a la interfície d'usuari, al Batch i a l'AssetStorage, que romandrà buit fins que la LoadingScreen no carregui els assets.

Cal tenir en compte que totes les pantalles tenen una referència a la instància de ~Game~, fent molt fàcil l'acoplament amb la seva implementació. Bàsicament, qualsevol propietat pública dins de ~Game~ actua com una variable quasi global. Limitar l'accés a aquestes variables des de l'ECS ha estat una de les lliçons més dures a l'hora de programar el joc.

*** DONE LoadingScreen
Només inicialitzar-se, ~Game~ carregará la ~LoadingScreen~, encarregada de carregar asíncronament la majoria dels assets que no requereixin una gran quantitat de memòria, com la música.

Les pantalles de càrrega s'encarreguen de mostrar el progrés mentre s'inicia el joc, donant feedback a l'usuari i mostrant informació d'interés.
*** DONE GamePlayScreen
La pantalla més important del joc. Aquesta classe s'encarrega d'avançar el loop principal del joc, i d'inicialitzar la partida.

Conté la instància d'~Engine~, la classe principal de l'ECS. La resta de referéncies que pugui haber contingut la ~GamePlayScreen~ han estat refactoritzades, mogudes a dins l'ECS per a limitar l'accés i simplificar enormement la classe.

Cada tick del loop principal es fa una crida a ~engine.update(delta)~ i a ~AudioLocator.service.update()~.

** TODO L'ECS
Entés com al conjunt d'implementacions de *components* i *sistemes*, és la peça de codi que més iteracions a vist de tot el programa. A més refactorització, més evident es torna la diferència entre la orientació a dades i la orientació a objectes, que desafia tota intuició inculcada sobre com programar un sistema complex i a l'hora mantenible.

No es pot descriure un sistema compost sense descriure les seves parts, per les quals és definit. No es pot descriure una part d'un tot sense el seu context; careix de significat. En comptes d'explicar els *sistemes* o els *components* com a entitats independents, explicaré cada seqüència d'interaccions reificant l'acció com a conjunt de dades i subrutines sobre aquestes.

Abans de procedir a explicar cada procés, introduiré la inicialització del motor, ~PooledEngine~:

#+begin_src kotlin
    private val engine = PooledEngine().apply {
        addSystem(PhysicsStepSystem())
        addSystem(ContactEventSystem())
        addSystem(HealthSystem())
        addSystem(KeyboardInputSystem())
        addSystem(MouseInputSystem(game.gameViewport))
        addSystem(FiringSystem(game))
        addSystem(ThrustingSystem())
        addSystem(CameraSystem(game.gameViewport))
        addSystem(
            RenderSystem(
                game.batch, game.gameViewport, game.uiViewport, listOf(
                    game.assets[BackgroundAsset.NEBULA.descriptor],
                    game.assets[BackgroundAsset.SPACE_LAYER_0.descriptor],
                    game.assets[BackgroundAsset.SPACE_LAYER_3.descriptor],
                    game.assets[BackgroundAsset.STARFIELD2.descriptor],
                )
            )
        )
        addSystem(RemoveSystem())
    }
#+end_src

L'ordre en el que l'~engine~ itera sobre els *sistemes* és mateix en el que son afegits. Només de mirar el codi podem saber exactament el que passa i quan passa a cada pas del programa, i si és necessari afegir un nou comportament al joc només cal afegir un nou ~Entitysystem~ que gràcies a les restriccions imposades pel model no se solaparà amb la resta.

*** TODO La simulació
La classe ~World~ és la que conté totes les referències als cossos rigids. Cridant ~World.step()~ s'avança la simulació física.

La intuició indica prudent guardar una referència a ~world~ a dins d'un *sistema*, i això seria un gran error. Recordem els dos primers principis que regeixen la programació d'un sistema ECS: els *sistemes* no tenen estat, els *components* no tenen comportament. Guardar una referència a un objecte l'estat del qual es transmet d'un tick del joc al següent a dins d'un *sistema* crearia dependencies des d'aquest sistema cap a qualsevol altre objecte que necessites accedir a l'estat (en aquest cas la simulació), i a partir d'aquell moment modificar el comportament del sistema es tornaria una font segura d'errors.

Així doncs, només queden dues opcions: Guardar la simulació a fora de l'ECS o guardar-la a dins d'un *Component*. La primera opció és la més sencilla a primera vista, però degut a l'estructura del projecte aquesta instància es comportaria com una variable global en tota regla.

La segona opció es tracta de moure tota aquella informació que intuitivament residiria a dins d'un *sistema* a dins d'un *component* que només s'instanciarà un cop. Aquest *component* seria referenciable a través d'una entitat, que idealment seria accessible a través del gestor de l'ECS. Així compliriem amb els principis d'un ECS: els sistemes no tenen estat, els components no tenen comportament. Aquest patrò s'anomena *Singleton Component*, i és necessari per a desacoplar els sistemes entre si.

~PhysicsStepSystem~ va ser el primer *sistema* la informació del qual es va refactoritzar com a singleton component, i va ser clau per a la futura gestió dels events provinents de contactes físics.

*** TODO Input d'usuari i comunicació amb la simulació
*** TODO La renderització
**** TODO Dos viewports
*** TODO La nau dispara
*** TODO Els contactes físics
*** TODO Suspicàcia: els events no encaixen
*** TODO Eliminació d'una entitat

* TODO Algorismes, jocs de miralls
** TODO Rerefons amb Paral·laxi
*** TODO Per què paral·laxi, i no una camara amb perspectiva?
** TODO Efectes sonors i música
** TODO L'espiral de la mort (Fix your timestep)

* TODO Jocs en xarxa
*** TODO El determinisme en xarxa
*** TODO El temps

* TODO Bibliografia
** TODO Floating point arithmetics:
- https://yosefk.com/blog/consistency-how-to-defeat-the-purpose-of-ieee-floating-point.html
- https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html
** TODO Patrons
- [[www.gameprogrammingpatterns.com][Game Programming Patterns]]
** TODO Arquitectura:
[[https://www.youtube.com/watch?v=W3aieHjyNvw&t=735s][Overwatch: Architecture and netcode]]
** TODO Spacewars!
- https://www.retromobe.com/2020/11/dec-pdp-1-1960.html
** TODO Imatges
- ECS-process.png: https://scribe.rip/@clevyr/entity-component-system-for-react-js-e3ab6e9be776
- ECS-shape.png: https://merwanachibet.net/blog/switching-our-rts-to-the-entity-component-system-pattern-ecs/

* TODO Atribucions
Els programadors mai ho fem tot sols. I no parlo només de l'escriptura del codi en sí. Disenyadors gràfics i dibuixants, artistes tipogràfics i músics han aportat el seu granet de sorra a tots i cada un dels videojocs existents. Aquest projecte no n'és l'excepció, i és just que els hi ho agraeixi atribuint-los el seu esforç.
Els desenvolupadors mai ho fem tot sols. I no parlo només de l'escriptura del codi en sí. Disenyadors gràfics i dibuixants, artistes tipogràfics i músics han aportat el seu granet de sorra a tots i cada un dels videojocs existents. Aquest projecte no n'és l'excepció, i és just que els hi ho agraeixi atribuint-los el seu esforç.

Tots els recursos utilitzats a Gravity Mayhem han estat o bé lliurats al domini públic o bé llicenciats amb llicencies permissives. Aquest joc mateix està llicenciat amb la llicencia MIT, que dona total llibertat al seu ús, distribució, edició i distribució de derivats en qualsevol forma, gratuïta i comercial.

*** TODO Recursos gràfics
*** TODO Audio
