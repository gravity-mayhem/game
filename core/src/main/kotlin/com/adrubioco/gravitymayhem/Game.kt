package com.adrubioco.gravitymayhem

import com.adrubioco.gravitymayhem.screens.LoadingScreen
import com.badlogic.gdx.Application
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.viewport.FitViewport
import ktx.app.KtxGame
import ktx.app.KtxScreen
import ktx.assets.async.AssetStorage
import ktx.async.KtxAsync
import ktx.log.logger

private val LOG = logger<Game>()

/*
    These particular camera sizes are this way so at minimum zoom level
    one virtual pixel fills vertically all of the screen.
 */
const val CAMERA_WIDTH: Float = 1.6f + 1.6f * ((1f - 0.9f) / 0.9f)
const val CAMERA_HEIGHT: Float = 1f
const val INIT_CAMERA_ZOOM = 32f
const val V_WIDTH_PIXELS = 1920f
const val V_HEIGHT_PIXELS = 1080f
const val DO_DEBUG = false

class Game : KtxGame<KtxScreen>(clearScreen = true) {
    val gameViewport by lazy {
        FitViewport(CAMERA_WIDTH, CAMERA_HEIGHT).apply {
            (camera as OrthographicCamera).zoom = INIT_CAMERA_ZOOM
        }
    }
    val uiViewport by lazy { FitViewport(V_WIDTH_PIXELS, V_HEIGHT_PIXELS) }
    val batch: Batch by lazy { SpriteBatch() }
    val assets: AssetStorage by lazy { AssetStorage() }

    override fun create() {
        Gdx.app.logLevel = if (DO_DEBUG) Application.LOG_DEBUG else Application.LOG_NONE
        LOG.debug { "Create game instance" }
        KtxAsync.initiate()
        addScreen(LoadingScreen(this))
        setScreen<LoadingScreen>()
    }

    override fun dispose() {
        super.dispose()
        LOG.debug { "Disposed sprites in batch: ${(batch as SpriteBatch).maxSpritesInBatch}" }
        batch.dispose()
        assets.dispose()
    }

}