package com.adrubioco.gravitymayhem.ashley

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import ktx.ashley.allOf
import kotlin.reflect.KClass

fun Engine.singleton(component: KClass<out Component>): Entity {
    return this.getEntitiesFor(allOf(component).get()).first()
}