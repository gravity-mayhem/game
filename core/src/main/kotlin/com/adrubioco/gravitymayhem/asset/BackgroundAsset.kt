package com.adrubioco.gravitymayhem.asset

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.Texture

enum class BackgroundAsset(
    fileName: String,
    directory: String = "graphics/backgrounds",
    val descriptor: AssetDescriptor<Texture> =
        AssetDescriptor("$directory/$fileName", Texture::class.java)
) {
    SPACE_LAYER_0("Space_layer_0.png"),
    SPACE_LAYER_1("Space_layer_1.png"),
    SPACE_LAYER_2("Space_layer_2.png"),
    SPACE_LAYER_3("Space_layer_3.png"),
    SPACE_LAYER_4("Space_layer_4.png"),
    SPACE_LAYER_MINISTARS1("Space_layer_miniStars.png"),
    SPACE_LAYER_MINISTARS2("Space_layer_miniStars2.png"),
    SPACE_LAYER_UPPER("Space_layer_upper.png"),
    CLOUD_MASK_1("stars_cloud_mask.png"),
    CLOUD_MASK_2("stars_cloud_mask_2.png"),
    CLOUD_MASK_2B("stars_cloud_mask_2b.png"),
    CLOUD_MASK_3("stars_cloud_mask_3.png"),

    STARFIELD("Starfield-8-1024x1024.png"),
    STARFIELD2("StarSky01.png"),
    NEBULA("Blue-Nebula-1-1024x1024.png")
}
