package com.adrubioco.gravitymayhem.asset

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.utils.I18NBundle

enum class I18NBundleAsset(
    fileName: String,
    directory: String = "i18n",
    val descriptor: AssetDescriptor<I18NBundle> = AssetDescriptor("$directory/$fileName", I18NBundle::class.java)
) {
}
