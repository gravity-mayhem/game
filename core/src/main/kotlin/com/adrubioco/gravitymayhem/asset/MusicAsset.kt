package com.adrubioco.gravitymayhem.asset

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.audio.Music

enum class MusicAsset(
    fileName: String,
    directory: String = "music",
    val descriptor: AssetDescriptor<Music> = AssetDescriptor("$directory/$fileName", Music::class.java)
) {
    MENU("scifi-ambient.mp3"),
    SPACEMIX("spacemix.mp3")
}
