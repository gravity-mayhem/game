package com.adrubioco.gravitymayhem.asset

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.audio.Sound

enum class SoundAsset(
    fileName: String,
    directory: String = "sound",
    val descriptor: AssetDescriptor<Sound> = AssetDescriptor("$directory/$fileName", Sound::class.java)
) {
    BUILD2("build2.mp3"),
//    METAL_COLLISION(""),
//    ROCK_COLLISION(""),
//    BULLET_SOUND1(""),
}
