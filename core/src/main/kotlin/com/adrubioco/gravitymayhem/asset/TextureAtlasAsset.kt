package com.adrubioco.gravitymayhem.asset

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion


enum class TextureAtlasAsset(
    val isSkinAtlas: Boolean,
    fileName: String,
    directory: String = "graphics",
    val descriptor: AssetDescriptor<TextureAtlas> = AssetDescriptor("$directory/$fileName", TextureAtlas::class.java)
) {
    GAME_GRAPHICS(false, "game_graphics.atlas"),
    UI(true, "ui.atlas", "ui")
}

fun TextureAtlas.findRegion(texture: GameGraphicsTexture): TextureRegion {
    return this.findRegion(texture.name)
}

/**
 *  Names of the sprites inside the texture atlas
 *  With the existing extension properties like TextureAtlas.ball, you can use
 *  graphicsAtlas.ball instead of graphicsAtlas.findRegion(GameGraphicsTextures.ball.name)
 *  */
enum class GameGraphicsTexture {
    Spaceship_06_ORANGE, ball, laser
}

/**
 * TODO: Use Clojure or scala to generate class extensions like this one from the name of every texture inside an atlas
 */
val TextureAtlas.Spaceship_06_ORANGE: TextureRegion
    get() {
        return this.findRegion(GameGraphicsTexture.Spaceship_06_ORANGE)
    }

val TextureAtlas.ball: TextureRegion
    get() {
        return this.findRegion(GameGraphicsTexture.ball)
    }


val TextureAtlas.laser: TextureRegion
    get() {
        return this.findRegion(GameGraphicsTexture.laser)
    }

enum class UITexture
