package com.adrubioco.gravitymayhem.audio

/**
 * AudioLocator needs to be supplied an Audio implementation.
 *
 * It uses the Locator pattern to supply a service
 * which could change implementation at any moment.
 */
object AudioLocator {
    var service: Audio = NullAudio()
}

//class AudioLocator
//{
//    public:
//    static void initialize() { service_ = &nullService_; }
//
//    static Audio& getAudio() { return *service_; }
//
//    static void provide(Audio* service)
//    {
//        if (service == NULL)
//        {
//            // Revert to null service.
//            service_ = &nullService_;
//        }
//        else
//        {
//            service_ = service;
//        }
//    }
//
//    private:
//    static Audio* service_;
//    static NullAudio nullService_;
//};
