package com.adrubioco.gravitymayhem.box2d

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import ktx.math.vec2

enum class Material {
    METAL,
    ROCK,
    ETHER // The "no material" material
}

/**
 * Simple container for extended physical data about a Body.
 * It also provides extension functions to access properties
 * of the UserData as if they were declared on the Body.
 */
class UserData(
    var entity: Entity? = null,
    var material: Material
)

val Body.data: UserData
    get() = this.userData as UserData
