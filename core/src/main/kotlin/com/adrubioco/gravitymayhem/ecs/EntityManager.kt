package com.adrubioco.gravitymayhem.ecs

import com.adrubioco.gravitymayhem.box2d.Material
import com.adrubioco.gravitymayhem.box2d.UserData
import com.adrubioco.gravitymayhem.ecs.components.*
import com.adrubioco.gravitymayhem.util.PhysicalProps
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.Shape
import com.badlogic.gdx.physics.box2d.World
import ktx.ashley.entity
import ktx.ashley.with
import ktx.box2d.body
import ktx.box2d.box
import ktx.math.vec2

class EntityManager {

    companion object {

        fun createPlayerShip(
            engine: Engine,
            world: World,
            physicalProps: PhysicalProps,
            region: TextureRegion
        ): Entity {
            return engine.entity {
                with<HumanPlayerComponent>()
                with<CommandComponent>()
                with<ThrustComponent> {
                    setup(10f, 10f, 10f, 0.80f, 25f)
                }
                with<RigidBodyComponent> {
                    setup(
                        world = world,
                        this@entity.entity,
                        physicalProps = physicalProps,
                    )
                }
                with<CameraFocusComponent>()
                with<SpriteComponent> {
                    setup(region, physicalProps.size)
                }
                with<HealthComponent> {
                    setup(0f, 100f)
                }
                with<WeaponComponent> {
                    setup(damage = 5f, fireRate = 0.2f)
                }
            }
        }

        fun createBullet(
            engine: Engine, world: World,
            physicalProps: PhysicalProps,
            region: TextureRegion
        ): Entity {
            return engine.entity {
                with<RigidBodyComponent> {
                    setup(
                        world,
                        this@entity.entity,
                        physicalProps,
                        true
                    )
                    body.linearVelocity = physicalProps.velocity
                }
                with<SpriteComponent> {
                    setup(region, physicalProps.size)
                }
                with<HealthComponent> {
                    setup(0f, 1f)
                }
                with<Bullet> {
                    setup(5f, BulletType.KINETIC)
                }
            }
        }

        fun createBall(
            engine: Engine,
            world: World,
            physicalProps: PhysicalProps,
            region: TextureRegion
        ): Entity {
            return engine.entity {
                with<RigidBodyComponent> {
                    setup(
                        world,
                        this@entity.entity,
                        physicalProps,
                        shape = Shape.Type.Circle
                    )
                }
                with<SpriteComponent> {
                    setup(region, physicalProps.size)
                }
                with<HealthComponent> {
                    setup(1f, 400f)
                }
            }
        }

        fun createBouncyKineticBox(engine: Engine, world: World, w: Float, h: Float, x: Float, y: Float): Entity {
            return engine.entity {
                with<RigidBodyComponent> {
                    body = world.body(type = BodyDef.BodyType.KinematicBody) {
                        this.userData = UserData(this@entity.entity, Material.ETHER)
                        box(width = w, height = h, position = vec2(x, y)) {
                            restitution = 1f
                        }
                    }
                }
            }
        }

        fun createWorldLimits(engine: Engine, world: World, width: Float, height: Float) {
            this.createBouncyKineticBox(engine, world, width, 1f, 0f, height * 0.5f)
            this.createBouncyKineticBox(engine, world, width, 1f, 0f, -height * 0.5f)
            this.createBouncyKineticBox(engine, world, 1f, height, width * 0.5f, 0f)
            this.createBouncyKineticBox(engine, world, 1f, height, -width * 0.5f, 0f)
        }
    }

}