package com.adrubioco.gravitymayhem.ecs.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool
import ktx.ashley.mapperFor

enum class BulletType {
    KINETIC,
    EXPLOSIVE,
    LASER,
    IMPLOSIVE
}

class Bullet : Component, Pool.Poolable {

    var damage: Float = 1f
    var bulletType: BulletType = BulletType.KINETIC

    override fun reset() {}

    fun setup(damage: Float, type: BulletType) {
        this.damage = damage
        this.bulletType = type
    }

    companion object {
        val mapper = mapperFor<Bullet>()
    }
}
