package com.adrubioco.gravitymayhem.ecs.components


import com.badlogic.ashley.core.Component
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Pool
import ktx.ashley.mapperFor

/**
 * Contains the player's intent,
 * as in "I want this ship to thrust forward"
 * or "I want this ship to rotate this way".
 */
class CommandComponent(
    var stabilize: Boolean = false,
    val facingDirection: Vector2 = Vector2.X,
    var goForward: Boolean = false,
    var goBackwards: Boolean = false,
    var goLeft: Boolean = false,
    var goRight: Boolean = false,
    var rotateLeft: Boolean = false,
    var rotateRight: Boolean = false,
    var burn: Boolean = false,
    var firePrimaryWeapon: Boolean = false
) : Component, Pool.Poolable {

    override fun reset() {
        stabilize = false
        facingDirection.set(Vector2.X)
        goForward = false
        goBackwards = false
        goLeft = false
        goRight = false
        rotateLeft = false
        rotateRight = false
    }

    companion object {
        val mapper = mapperFor<CommandComponent>()
    }
}
