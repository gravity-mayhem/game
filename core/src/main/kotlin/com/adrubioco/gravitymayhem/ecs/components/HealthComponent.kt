package com.adrubioco.gravitymayhem.ecs.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool
import ktx.ashley.mapperFor
import kotlin.properties.Delegates

/**
 * @param timeAccumulator used to
 */
class HealthComponent(
    var regenRatePerSecond: Float = 0f,
    var maxHealth: Float = 100f
) : Component, Pool.Poolable {
    var health: Float by Delegates.observable(100f) {
            _, _, new ->
        if (new > maxHealth) this.health = maxHealth
    }

    override fun reset() {
        this.health = 9999f
    }

    fun setup(regenRatePerSecond: Float, maxHealth: Float) {
        this.regenRatePerSecond = regenRatePerSecond
        this.maxHealth = maxHealth
        this.health = maxHealth
    }

    companion object {
       val mapper = mapperFor<HealthComponent>()
    }
}