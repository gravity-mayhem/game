package com.adrubioco.gravitymayhem.ecs.components

import com.adrubioco.gravitymayhem.box2d.UserData
import com.adrubioco.gravitymayhem.shared.box2d.Y_UP
import com.adrubioco.gravitymayhem.util.PhysicalProps
import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.Shape
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.Pool
import ktx.ashley.mapperFor
import ktx.box2d.body
import ktx.box2d.box
import ktx.box2d.circle

class RigidBodyComponent : Component, Pool.Poolable {

    lateinit var body: Body

    override fun reset() {
        body.fixtureList.forEach {
            body.destroyFixture(it)
        }
        body.world.destroyBody(body)
    }

    fun setup(
        world: World,
        entity: Entity,
        physicalProps: PhysicalProps,
        isBullet: Boolean = false,
        shape: Shape.Type? = null
    ) {
        body = world.body(type = BodyDef.BodyType.DynamicBody) {
            this.position.set(physicalProps.pos)
            this.angle = Y_UP + physicalProps.angle
            this.userData = UserData(entity, physicalProps.material)

            when (shape) {
                Shape.Type.Circle -> circle(radius = physicalProps.size.x / 2) {
                    this.density = physicalProps.density
                }
//                Shape.Type.Edge -> TODO()
//                Shape.Type.Polygon -> TODO()
//                Shape.Type.Chain -> TODO()
                else -> box(physicalProps.size.y, physicalProps.size.x) {
                    this.density = physicalProps.density
                }
            }

        }.also {
            it.isBullet = isBullet
        }
    }

    companion object {
        val mapper = mapperFor<RigidBodyComponent>()
    }
}