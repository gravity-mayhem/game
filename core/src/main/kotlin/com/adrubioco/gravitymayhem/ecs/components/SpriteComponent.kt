package com.adrubioco.gravitymayhem.ecs.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Pool
import ktx.ashley.mapperFor
import ktx.log.logger

private val LOG = logger<SpriteComponent>()

class SpriteComponent(val sprite: Sprite = Sprite()) : Component, Pool.Poolable {

    override fun reset() {
        LOG.debug { "Resetting" }
        sprite.apply {
            texture = null
            setColor(1f, 1f, 1f, 1f)
            setSize(0f, 0f)
            setOriginCenter()
        }
    }

    fun setup(region: TextureRegion, size: Vector2) {
        sprite.apply {
            setRegion(region)
            setSize(size.x, size.y)
            setOriginCenter()
        }
    }

    companion object {
        val mapper = mapperFor<SpriteComponent>()
    }

}
