package com.adrubioco.gravitymayhem.ecs.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool
import ktx.ashley.mapperFor

/* Forces in newtons */
class ThrustComponent(
    var maxForwardForce: Float = 4f,
    var maxSidewaysForce: Float = 4f,
    var maxBackwardsForce: Float = 4f,
    var maxTorque: Float = 0.70f,
    var maxBurnForce: Float = 20f
) : Component, Pool.Poolable {

    override fun reset() {}

    fun setup(
        maxForwardForce: Float = this.maxForwardForce,
        maxSidewaysForce: Float = this.maxSidewaysForce,
        maxBackwardsForce: Float = this.maxBackwardsForce,
        maxTorque: Float = this.maxTorque,
        maxBurnForce: Float = this.maxBurnForce
    ) {
        this.maxForwardForce = maxForwardForce
        this.maxSidewaysForce = maxSidewaysForce
        this.maxBackwardsForce = maxBackwardsForce
        this.maxTorque = maxTorque
        this.maxBurnForce = maxBurnForce
    }


    companion object {
        val mapper = mapperFor<ThrustComponent>()
    }
}