package com.adrubioco.gravitymayhem.ecs.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.Pool
import ktx.ashley.mapperFor
import ktx.log.logger

private val LOG = logger<TransformComponent>()

class TransformComponent(
    val position: Vector3 = Vector3(Vector3.Zero), //Scalar quantity
    val size: Vector2 = Vector2(1f, 1f), //Scalar quantity
    var rotationRad: Float = 0f
) : Component, Pool.Poolable, Comparable<TransformComponent> {

    override fun reset() {
        position.set(Vector3.Zero)
        size.set(1f, 1f)
        rotationRad = 0f
    }

    override fun compareTo(other: TransformComponent): Int {
        val zDiff = other.position.z.compareTo(position.z)
        return if (zDiff == 0) other.position.y.compareTo(position.y) else zDiff
    }

    companion object {
        val mapper = mapperFor<TransformComponent>()
    }

}