package com.adrubioco.gravitymayhem.ecs.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool
import ktx.ashley.mapperFor

private const val DEFAULT_DAMAGE = 5f
private const val DEFAULT_FIRE_RATE = 1f // In seconds

class WeaponComponent(
    var damage: Float = DEFAULT_DAMAGE,
    var timeSinceLastShoot: Float = DEFAULT_FIRE_RATE,
    var fireRate: Float = DEFAULT_FIRE_RATE
) : Component, Pool.Poolable {

    fun setup(damage: Float, fireRate: Float) {
        this.damage = damage
        this.fireRate = fireRate
    }
    override fun reset() {
        damage = DEFAULT_DAMAGE
        timeSinceLastShoot = DEFAULT_FIRE_RATE
        fireRate = DEFAULT_FIRE_RATE
    }

    fun isReadyToFire(): Boolean {
        return fireRate <= timeSinceLastShoot
    }

    companion object {
        val mapper = mapperFor<WeaponComponent>()
    }
}