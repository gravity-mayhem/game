package com.adrubioco.gravitymayhem.ecs.singleton

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.Disposable
import ktx.ashley.mapperFor
import ktx.math.vec2

object Simulation : Component, Disposable {
    var world: World = World(vec2(), false).apply {
        autoClearForces = false
    }

    override fun dispose() {
        world.dispose()
    }

    val mapper = mapperFor<Simulation>()
}