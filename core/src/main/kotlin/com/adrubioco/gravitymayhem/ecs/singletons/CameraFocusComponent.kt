package com.adrubioco.gravitymayhem.ecs.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool
import ktx.ashley.mapperFor
import ktx.log.logger

private val LOG = logger<CameraFocusComponent>()

class CameraFocusComponent : Component, Pool.Poolable {

    init {
        LOG.debug { "Created CameraComponent" }
    }

    override fun reset() = Unit

    companion object {
        val mapper = mapperFor<CameraFocusComponent>()
    }
}