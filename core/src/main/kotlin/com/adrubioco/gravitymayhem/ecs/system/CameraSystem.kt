package com.adrubioco.gravitymayhem.ecs.system

import com.adrubioco.gravitymayhem.ecs.components.CameraFocusComponent
import com.adrubioco.gravitymayhem.ecs.components.RigidBodyComponent
import com.adrubioco.gravitymayhem.shared.box2d.vectorAngle
import com.adrubioco.gravitymayhem.shared.math.set
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.utils.viewport.Viewport
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.log.logger

private val LOG = logger<CameraSystem>()

class CameraSystem(private val gameViewport: Viewport) : IteratingSystem(allOf(CameraFocusComponent::class).get()) {

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val camera = entity[CameraFocusComponent.mapper]
        require(camera != null) { "Entity |entity| must have a CameraComponent. entity=$entity" }
        val bodyComp = entity[RigidBodyComponent.mapper]
        require(bodyComp != null) { "Entity |entity| must have a RigidBodyComponent. entity=$entity" }

        val body = bodyComp.body

        val up = body.vectorAngle()

        gameViewport.camera.up.set(up)
        gameViewport.camera.position.set(bodyComp.body.position)
        gameViewport.camera.update()
    }

}