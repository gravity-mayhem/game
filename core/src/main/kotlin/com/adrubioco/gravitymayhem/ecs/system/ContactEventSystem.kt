package com.adrubioco.gravitymayhem.ecs.system

import com.adrubioco.gravitymayhem.asset.SoundAsset
import com.adrubioco.gravitymayhem.audio.AudioLocator
import com.adrubioco.gravitymayhem.ecs.singleton.Simulation
import com.adrubioco.gravitymayhem.event.Event
import com.adrubioco.gravitymayhem.event.MainEventQueue
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.physics.box2d.Contact
import com.badlogic.gdx.physics.box2d.ContactImpulse
import com.badlogic.gdx.physics.box2d.ContactListener
import com.badlogic.gdx.physics.box2d.Manifold
import ktx.ashley.allOf
import ktx.log.logger

private val LOG = logger<ContactEventSystem>()

class ContactEventSystem : ContactListener, IteratingSystem(allOf(Simulation::class).get()) {

    override fun processEntity(entity: Entity, deltaTime: Float) {
        MainEventQueue.dispatchEvents()
    }

    override fun beginContact(contact: Contact) {
        MainEventQueue.enqueueEvent(Event.ContactEvent(contact))
        LOG.debug { "Event enqueued" }
        AudioLocator.service.play(SoundAsset.BUILD2)
    }

    override fun endContact(contact: Contact) = Unit
    override fun preSolve(contact: Contact, oldManifold: Manifold) = Unit
    override fun postSolve(contact: Contact, impulse: ContactImpulse) = Unit
}