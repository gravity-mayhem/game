package com.adrubioco.gravitymayhem.ecs.system

import com.adrubioco.gravitymayhem.ecs.components.CommandComponent
import com.adrubioco.gravitymayhem.ecs.components.HumanPlayerComponent
import com.adrubioco.gravitymayhem.ecs.components.RigidBodyComponent
import com.adrubioco.gravitymayhem.ecs.singleton.Simulation
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.viewport.Viewport
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.log.logger

private val LOG = logger<DebugSystem>()

class DebugSystem(
    private val viewport: Viewport,
    private val renderer: Box2DDebugRenderer =
        Box2DDebugRenderer(true, true, true, true, true, true)
) : IteratingSystem(
    allOf(Simulation::class).get()
) {

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val simulation = entity[Simulation.mapper]
        require(simulation != null) { "Entity |entity| must have a Simulation. entity=$entity" }

        renderer.render(simulation.world, viewport.camera.combined) //Render the hitboxes

        if (Gdx.input.isKeyJustPressed(Input.Keys.Q)) {
            (viewport.camera as OrthographicCamera).zoom *= 2
            LOG.debug { "Zoom: ${(viewport.camera as OrthographicCamera).zoom}" }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.E)) {
            (viewport.camera as OrthographicCamera).zoom /= 2
            LOG.debug { "Zoom: ${(viewport.camera as OrthographicCamera).zoom}" }
        }
    }

}

