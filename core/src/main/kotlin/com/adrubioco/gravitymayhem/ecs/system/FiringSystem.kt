package com.adrubioco.gravitymayhem.ecs.system

import com.adrubioco.gravitymayhem.Game
import com.adrubioco.gravitymayhem.asset.TextureAtlasAsset
import com.adrubioco.gravitymayhem.asset.laser
import com.adrubioco.gravitymayhem.box2d.Material
import com.adrubioco.gravitymayhem.ecs.EntityManager
import com.adrubioco.gravitymayhem.ecs.components.CommandComponent
import com.adrubioco.gravitymayhem.ecs.components.RemoveComponent
import com.adrubioco.gravitymayhem.ecs.components.RigidBodyComponent
import com.adrubioco.gravitymayhem.ecs.components.WeaponComponent
import com.adrubioco.gravitymayhem.ecs.singleton.Simulation.world
import com.adrubioco.gravitymayhem.shared.box2d.Y_UP
import com.adrubioco.gravitymayhem.shared.box2d.vectorAngle
import com.adrubioco.gravitymayhem.util.PhysicalProps
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import ktx.ashley.allOf
import ktx.ashley.exclude
import ktx.ashley.get
import ktx.log.logger
import ktx.math.plus
import ktx.math.times
import ktx.math.vec2

private val LOG = logger<FiringSystem>()

const val BULLET_VELOCITY = 50f

class FiringSystem(private val game: Game) :
    IteratingSystem(allOf(WeaponComponent::class, CommandComponent::class).exclude(RemoveComponent::class).get()) {

    private val defaultBulletProps = PhysicalProps(
        size = vec2(0.15f, 0.7f),
        density = 1f,
        material = Material.ETHER,
        pos = vec2(),
        angle = 0f,
        velocity = vec2(BULLET_VELOCITY)
    )

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val weapon = entity[WeaponComponent.mapper]
        require(weapon != null) { "Entity |entity| must have a WeaponComponent. entity=$entity" }
        val command = entity[CommandComponent.mapper]
        require(command != null) { "Entity |entity| must have a CommandComponent. entity=$entity" }

        weapon.timeSinceLastShoot += deltaTime
        if (command.firePrimaryWeapon && weapon.isReadyToFire()) {
            LOG.debug { "Trying to shoot" }
            entity.shoot()
        }
    }

    private fun Entity.shoot() {
        val body = this[RigidBodyComponent.mapper]!!.body

        defaultBulletProps.apply {
            pos.set(body.position + (body.vectorAngle() * size.y + body.vectorAngle() * 0.3f))
            velocity.set(BULLET_VELOCITY,0f)
            velocity.set(body.linearVelocity + velocity.setAngleRad(body.angle))
            angle = body.angle - Y_UP
        }

        EntityManager.createBullet(
            engine = engine,
            world = world,
            defaultBulletProps,
            region = game.assets[TextureAtlasAsset.GAME_GRAPHICS.descriptor].laser
        )

        this[WeaponComponent.mapper]!!.timeSinceLastShoot = 0f
    }
}

