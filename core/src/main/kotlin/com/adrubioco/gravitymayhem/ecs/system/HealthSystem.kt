package com.adrubioco.gravitymayhem.ecs.system

import com.adrubioco.gravitymayhem.ecs.components.HealthComponent
import com.adrubioco.gravitymayhem.ecs.components.RemoveComponent
import com.adrubioco.gravitymayhem.event.Event
import com.adrubioco.gravitymayhem.event.EventListener
import com.adrubioco.gravitymayhem.event.MainEventQueue
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import ktx.ashley.allOf
import ktx.ashley.exclude
import ktx.ashley.get
import ktx.log.logger

private val LOG = logger<HealthSystem>()

private val SPEED_TO_DAMAGE = 1f

class HealthSystem : EventListener,
    IteratingSystem(allOf(HealthComponent::class).exclude(RemoveComponent::class).get()) {

    init {
        MainEventQueue.addListener(Event.ContactEvent::class, this@HealthSystem)
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val healthComp = entity[HealthComponent.mapper]

//        LOG.debug { "Ship health is ${healthComp.health}" }
        require(healthComp != null) { "Entity |entity| must have an HealthComponent. entity=$entity" }

        with(healthComp) {
            if (healthComp.health < 0) {
                entity.add(RemoveComponent())
                MainEventQueue.enqueueEvent(Event.EntityDestroyedEvent(entity))
                LOG.debug { "Entity $entity should be destroyed" }
            } else {
                healthComp.health += deltaTime * regenRatePerSecond
            }
        }
    }

    override fun onEvent(event: Event) {
        when (event) {
            is Event.ContactEvent -> {
                LOG.debug { "Entity collided" }
                dealContactDamage(event)
            }
            else -> {}
        }
    }

    private fun dealContactDamage(event: Event.ContactEvent) {
        val speed = event.impactVelocity.len()
        event.entityA[HealthComponent.mapper]?.let {
            it.health -= speed * SPEED_TO_DAMAGE
        }
        event.entityB[HealthComponent.mapper]?.let {
            it.health -= speed * SPEED_TO_DAMAGE
        }
    }

}