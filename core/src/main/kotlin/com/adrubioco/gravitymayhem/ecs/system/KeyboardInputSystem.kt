package com.adrubioco.gravitymayhem.ecs.system

import com.adrubioco.gravitymayhem.ecs.components.CommandComponent
import com.adrubioco.gravitymayhem.ecs.components.HumanPlayerComponent
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.log.logger

private val LOG = logger<KeyboardInputSystem>()

/**
 * This system updates the PlayerInputComponent with the player's (human or otherwise) input.
 * For this, it needs to retrieve where the object (eg. an spaceship) is at any given moment from the transform component,
 * and where it wants to go (current mouse position, for example).
 */
class KeyboardInputSystem : IteratingSystem(
    allOf(CommandComponent::class, HumanPlayerComponent::class).get()
) {
    override fun processEntity(entity: Entity, deltaTime: Float) {
        val playerInput = entity[CommandComponent.mapper]
        require(playerInput != null) { "Entity |entity| must have a PlayerInputComponent. entity=$entity" }

        with(Gdx.input) {
            playerInput.burn = isKeyPressed(Input.Keys.I)
            playerInput.goForward = isKeyPressed(Input.Keys.W)
            playerInput.goLeft = isKeyPressed(Input.Keys.A)
            playerInput.goRight = isKeyPressed(Input.Keys.D)
            playerInput.goBackwards = isKeyPressed(Input.Keys.S)
            playerInput.rotateLeft = isKeyPressed(Input.Keys.J)
            playerInput.rotateRight = isKeyPressed(Input.Keys.L)
            playerInput.stabilize = isKeyPressed(Input.Keys.H)
            playerInput.firePrimaryWeapon = isKeyPressed(Input.Keys.SPACE)
        }
    }

}
