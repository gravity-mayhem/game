package com.adrubioco.gravitymayhem.ecs.system

import com.adrubioco.gravitymayhem.ecs.components.CommandComponent
import com.adrubioco.gravitymayhem.ecs.components.HumanPlayerComponent
import com.adrubioco.gravitymayhem.ecs.components.RigidBodyComponent
import com.adrubioco.gravitymayhem.shared.math.Point2
import com.adrubioco.gravitymayhem.shared.math.vec2
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.viewport.Viewport
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.log.logger
import ktx.math.vec2

private val LOG = logger<MouseInputSystem>()

class MouseInputSystem(
    private val gameViewport: Viewport
) : IteratingSystem(
    allOf(CommandComponent::class, HumanPlayerComponent::class, RigidBodyComponent::class).get()
) {
    private val playerToMouse = vec2()
    private val mouse = Point2()

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val command = entity[CommandComponent.mapper]
        require(command != null) { "Entity |entity| must have a PlayerInputComponent. entity=$entity" }
        val bodyComp = entity[RigidBodyComponent.mapper]
        require(bodyComp != null) { "Entity |entity| must have a RigidBodyComponent. entity=$entity" }

        /* Get the mouse's position inside our game. */
        mouse.set(Gdx.input.x.toFloat(), Gdx.input.y.toFloat())
        gameViewport.unproject(mouse)
        command.facingDirection.set(vec2(bodyComp.body.position, mouse)) /* Value always between 0 and TAU */
    }

}

