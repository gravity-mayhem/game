package com.adrubioco.gravitymayhem.ecs.system

import com.adrubioco.gravitymayhem.ecs.singleton.Simulation
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.physics.box2d.*
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.log.logger

private val LOG = logger<PhysicsStepSystem>()

private val MAX_DELTA_STEP = 0.01f

/**
 * This system interfaces with the box2d.World object passed as argument, and advances it's simulation.
 *
 * @param world The Box2D World. Nothing else will interface directly with it.
 * @param worldEventQueue The queue which will decouple Contact events from the time of their handling. A WORLD'S SIMULATION STEP MUST NOT BE INTERRUPTED.
 */
class PhysicsStepSystem : IteratingSystem(allOf(Simulation::class).get()) {
    private var accumulator = 0f

    private fun physicsStep(world: World, deltaTime: Float) {
        accumulator += deltaTime
        val isThereTimeForOneMoreStep = { accumulator >= MAX_DELTA_STEP }
        val physicsSubstep = {
            world.step(MAX_DELTA_STEP, 6, 2)
            accumulator -= MAX_DELTA_STEP
        }

        while (isThereTimeForOneMoreStep()) {
            physicsSubstep()
        }
        world.clearForces()
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val simulation = entity[Simulation.mapper]
        require(simulation != null) { "Entity |entity| must have a Simulation. entity=$entity" }

        physicsStep(simulation.world, deltaTime)
    }

}