package com.adrubioco.gravitymayhem.ecs.system

import com.adrubioco.gravitymayhem.ecs.components.*
import com.adrubioco.gravitymayhem.shared.box2d.Y_UP
import com.adrubioco.gravitymayhem.shared.math.TAU
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.SortedIteratingSystem
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.MathUtils.radiansToDegrees
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.viewport.Viewport
import ktx.ashley.allOf
import ktx.ashley.exclude
import ktx.ashley.get
import ktx.graphics.use
import ktx.log.logger
import ktx.math.minus
import ktx.math.times
import ktx.math.vec2
import kotlin.math.absoluteValue
import kotlin.math.pow
import kotlin.math.sqrt

private val LOG = logger<RenderSystem>()

const val BG_DISTANCE_MULTIPLIER = 0.60f

class RenderSystem(
    private val batch: Batch,
    private val gameViewport: Viewport,
    private val uiViewport: Viewport,
    backgroundTextures: List<Texture>
) : SortedIteratingSystem(allOf(SpriteComponent::class, RigidBodyComponent::class).exclude(RemoveComponent::class)
    .get(), compareBy { entity -> entity[TransformComponent.mapper] }) {
    private val screenDiagonal = sqrt(
        uiViewport.worldWidth.toDouble().pow(2.0) + uiViewport.worldHeight.toDouble().pow(2.0)
    ).toFloat()
    private val newPos = vec2()
    private val temp = vec2()
    private val backgroundLayers: List<Sprite> = backgroundTextures.map { setupBackground(it) }

    private fun setupBackground(texture: Texture): Sprite {
        return Sprite(texture.apply {
            setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat)
        }).apply {
            LOG.debug { "$screenDiagonal" }
            setSize(screenDiagonal, screenDiagonal)
            setOriginCenter()
            setCenter(0f, 0f)
            scroll(1f, 1f)
        }
    }

    private fun renderBackgrounds(deltaTime: Float, viewport: Viewport, backgrounds: List<Sprite>) {
        viewport.apply()

        val bodyVelocity = vec2()
        var bodyAngle = 0f
        val cameraZoom = (gameViewport.camera as OrthographicCamera).zoom

        with(engine.getEntitiesFor(allOf(CameraFocusComponent::class).get())) {
            if (size() > 0) {
                first()[RigidBodyComponent.mapper]?.body?.let { body ->
                    bodyVelocity.set(body.linearVelocity)
                    bodyAngle = body.angle

                }
            }
        }

        with(backgrounds) {
            backgrounds.forEachIndexed { layer, background ->
                background.rotation = (TAU - bodyAngle + Y_UP) * radiansToDegrees

                val textureSize = background.texture.size()
                val textureSizeMultipliers = temp.set(
                    (screenDiagonal - textureSize.x) / textureSize.x,
                    (screenDiagonal - textureSize.y) / textureSize.y,
                )

                val layerDistance = backgrounds.size - layer + 2

                var xScroll = (deltaTime * (bodyVelocity.x / layerDistance))
                xScroll /= cameraZoom
                xScroll += xScroll * textureSizeMultipliers.x
                xScroll *= BG_DISTANCE_MULTIPLIER

                var yScroll = (deltaTime * (bodyVelocity.y / layerDistance))
                yScroll /= cameraZoom
                yScroll += yScroll * textureSizeMultipliers.y
                yScroll *= BG_DISTANCE_MULTIPLIER

                background.scroll(xScroll, -yScroll)
            }

            batch.use(uiViewport.camera.combined) {
                forEach {
                    it.draw(batch)
                }
            }
        }
    }

    override fun update(deltaTime: Float) {
        this.forceSort()

        renderBackgrounds(deltaTime, uiViewport, backgroundLayers)

        gameViewport.apply()
        batch.use(gameViewport.camera.combined) {
            super.update(deltaTime)
        }
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val graphic = entity[SpriteComponent.mapper]
        require(graphic != null) { "Entity |entity| must have a GraphicComponent. entity=$entity" }
        require(graphic.sprite.texture != null) { "Entity has no texture for rendering" }

        val rigidBody = entity[RigidBodyComponent.mapper]
        val transform = entity[TransformComponent.mapper]
        require(rigidBody != null) {
            "Entity |entity| must have a RigidBodyComponent. entity=$entity"
        }

        val sprite = graphic.sprite
        newPos.set(rigidBody.body.position - sprite.getSize() * 0.5f)
        with(sprite) {
            rotation = (rigidBody.body.angle - Y_UP) * radiansToDegrees
            setPosition(newPos.x, newPos.y)
            draw(batch)
        }
    }

    private fun Sprite.getSize(): Vector2 {
        temp.set(this.width, this.height)
        return temp
    }

    private fun Texture.size(): Vector2 {
        temp.set(this.width.toFloat(), this.height.toFloat())
        return temp
    }
}