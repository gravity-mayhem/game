package com.adrubioco.gravitymayhem.ecs.system

import com.adrubioco.gravitymayhem.ecs.components.CommandComponent
import com.adrubioco.gravitymayhem.ecs.components.RigidBodyComponent
import com.adrubioco.gravitymayhem.ecs.components.ThrustComponent
import com.adrubioco.gravitymayhem.shared.math.circle
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.physics.box2d.Body
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.log.logger
import ktx.math.plus
import ktx.math.times
import ktx.math.vec2

private val LOG = logger<ThrustingSystem>()

/**
 * Applies all forces that result from logic external to the Box2D world,
 * like those resulting from player input or explosions.
 */
class ThrustingSystem : IteratingSystem(
    allOf(CommandComponent::class, RigidBodyComponent::class).get()
) {

    /* Used on calculating directions. Updated for every entity processed. */
    private val forwardVector = vec2(1f, 0f)
    private val backwardVector = vec2(1f, 0f)
    private val rightVector = vec2(1f, 0f)
    private val leftVector = vec2(1f, 0f)

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val command = entity[CommandComponent.mapper]
        requireNotNull(command) { "Entity |entity| must have a CommandComponentInput. entity=$entity" }
        val bodyComp = entity[RigidBodyComponent.mapper]
        requireNotNull(bodyComp) { "Entity |entity| must have a RigidBodyComponent. entity=$entity" }
        val thrustComp = entity[ThrustComponent.mapper]
        requireNotNull(thrustComp) { "Entity |entity| must have a ThrustingComponent. entity=$entity" }

        val body = bodyComp.body
        val bodyAngle = body.angle % circle /* In radians, between 0 and TAU */

        /* These vectors are always relative to the body.
        * Here we update them */
        forwardVector.setAngleRad(bodyAngle).nor()
        backwardVector.set(forwardVector).rotate90(1).rotate90(1)
        rightVector.set(forwardVector).rotate90(-1)
        leftVector.set(forwardVector).rotate90(1)

        body.linearDamping = if (command.stabilize) 2f else 0f
        body.angularDamping = if (command.stabilize) 2f else 0f

        /* Linear movement */
        var linearForce = vec2()
        if (command.burn) linearForce = forwardVector * thrustComp.maxBurnForce
        if (command.goForward) linearForce = forwardVector * thrustComp.maxForwardForce
        if (command.goBackwards) linearForce += backwardVector * thrustComp.maxBackwardsForce
        if (command.goRight) linearForce += rightVector * thrustComp.maxSidewaysForce
        if (command.goLeft) linearForce += leftVector * thrustComp.maxSidewaysForce
        body.applyForceToCenter(linearForce, true)

        /* Angular movement */
        when {
            command.rotateRight -> body.applyTorque(-thrustComp.maxTorque)
            command.rotateLeft -> body.applyTorque(thrustComp.maxTorque)
            else -> {}
        }
    }

}

/** Defaults the wake param of this function to true.
 * It's cleaner. */
private fun Body.applyTorque(torque: Float) {
    this.applyTorque(torque, true)
}
