package com.adrubioco.gravitymayhem.event

import com.adrubioco.gravitymayhem.box2d.*
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.Contact
import ktx.math.minus

sealed class Event {

    class ContactEvent(contact: Contact) : Event() {
        val bodyA: Body = contact.fixtureA.body
        val bodyB: Body = contact.fixtureB.body
        val impactVelocityA: Vector2 = bodyA.linearVelocity.cpy()
        val impactVelocityB: Vector2 = bodyB.linearVelocity.cpy()
        val entityA: Entity = bodyA.data.entity!!
        val entityB: Entity = bodyB.data.entity!!
        val impactVelocity: Vector2 = (impactVelocityA - impactVelocityB)

        override fun toString(): String {
            return "Impact between $bodyA and $bodyB with a force of $impactVelocityA"
        }
    }

    class EntityDestroyedEvent(val entity: Entity) : Event() {
        override fun toString(): String {
            return "Entity $entity has been destroyed"
        }
    }

}