package com.adrubioco.gravitymayhem.event

import com.adrubioco.gravitymayhem.event.EventListener
import com.badlogic.gdx.utils.ObjectMap
import ktx.collections.GdxSet
import ktx.log.logger
import kotlin.reflect.KClass

private val LOG = logger<MainEventQueue>()

interface EventListener {
    fun onEvent(event: Event)
}

object MainEventQueue {
    private val listeners = ObjectMap<KClass<out Event>, GdxSet<EventListener>>()

    private val eventQueue: ArrayDeque<Event> = ArrayDeque(10)

    fun enqueueEvent(contactEvent: Event) {
        eventQueue.add(contactEvent)
    }

    fun dispatchEvents() {
        eventQueue.forEach { event ->
            listeners[event::class]?.forEach { listener ->
                listener.onEvent(event)
            }
        }
        eventQueue.clear()
    }

    fun addListener(type: KClass<out Event>, listener: EventListener) {
        var eventListeners = listeners[type]
        if (eventListeners == null) {
            eventListeners = GdxSet()
            listeners.put(type, eventListeners)
        }

        if (eventListeners.add(listener)) {
            LOG.debug { "Adding listener of type $type: $listener" }
        } else {
            LOG.error { "Trying to add already existing listener of type $type: $listener" }
        }
    }

    fun removeListener(type: KClass<out Event>, listener: EventListener) {
        val eventListeners = listeners[type]
        when {
            eventListeners == null -> {
                LOG.error { "Trying to remove listener $listener from non-existing listeners of type $type" }
            }
            listener !in eventListeners -> {
                LOG.error { "Trying to remove non-existing listener of type $type: $listener" }
            }
            else -> {
                LOG.debug { "Removing listener of type $type: $listener" }
                eventListeners.remove(listener)
            }
        }
    }

}
