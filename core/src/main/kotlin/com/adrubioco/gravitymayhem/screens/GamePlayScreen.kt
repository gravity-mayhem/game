package com.adrubioco.gravitymayhem.screens

import com.adrubioco.gravitymayhem.Game
import com.adrubioco.gravitymayhem.asset.*
import com.adrubioco.gravitymayhem.audio.AudioLocator
import com.adrubioco.gravitymayhem.ecs.EntityManager
import com.adrubioco.gravitymayhem.ecs.singleton.Simulation
import com.adrubioco.gravitymayhem.ecs.singleton.Simulation.world
import com.adrubioco.gravitymayhem.ecs.system.*
import com.adrubioco.gravitymayhem.util.PhysicalProps
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.Application.LOG_DEBUG
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.math.MathUtils.random
import kotlinx.coroutines.launch
import ktx.ashley.entity
import ktx.ashley.with
import ktx.async.KtxAsync
import ktx.log.logger
import ktx.math.vec2
import kotlin.random.Random

private val LOG = logger<GamePlayScreen>()

private val shipProps = PhysicalProps(
    size = vec2(1f, 1f),
    pos = vec2(5f, 5f)
)
private val ballProperties = PhysicalProps(
    pos = vec2(-5f, -5f),
    size = vec2(7f, 7f),
)

class GamePlayScreen(game: Game) : GravityMayhemScreen(game) {
    private val engine: Engine = PooledEngine().apply {
        addSystem(PhysicsStepSystem())
        addSystem(ContactEventSystem())
        addSystem(HealthSystem())
        addSystem(KeyboardInputSystem())
        addSystem(MouseInputSystem(game.gameViewport))
        addSystem(FiringSystem(game))
        addSystem(ThrustingSystem())
        addSystem(CameraSystem(game.gameViewport))
        addSystem(
            RenderSystem(
                game.batch, game.gameViewport, game.uiViewport, listOf(
                    game.assets[BackgroundAsset.NEBULA.descriptor],
                    game.assets[BackgroundAsset.SPACE_LAYER_0.descriptor],
                    game.assets[BackgroundAsset.SPACE_LAYER_3.descriptor],
                    game.assets[BackgroundAsset.STARFIELD2.descriptor],
                )
            )
        )
        addSystem(RemoveSystem())
    }.apply {
        entity {
            with<Simulation>().also {
                it.world.setContactListener(engine.getSystem(ContactEventSystem::class.java))
            }
        }
    }
    private val graphicsAtlas = game.assets[TextureAtlasAsset.GAME_GRAPHICS.descriptor]

    override fun show() {
        LOG.debug { "Game play screen shown" }

        if (Gdx.app.logLevel == LOG_DEBUG) engine.addSystem(DebugSystem(game.gameViewport))

        createEntities()

        playMusic()
    }

    private fun playMusic() {
        val old = System.currentTimeMillis()
        val musicAsset = MusicAsset.SPACEMIX
        val music = game.assets.loadAsync(musicAsset.descriptor)
        KtxAsync.launch {
            music.join()
            if (game.assets.isLoaded(musicAsset.descriptor)) {
                // music was really loaded and did not get unloaded already by the hide function
                LOG.debug { "It took ${(System.currentTimeMillis() - old) * 0.001f} seconds to load the $music" }
                AudioLocator.service.play(musicAsset)
            }
            AudioLocator.service.play(MusicAsset.SPACEMIX, 1f)
        }
    }

    private fun createEntities() {
        EntityManager.createPlayerShip(
            engine, world,
            shipProps,
            graphicsAtlas.Spaceship_06_ORANGE
        )

        repeat(30) {
            ballProperties.pos.set(
                Random.nextInt(-50, 50).toFloat(),
                Random.nextInt(-50, 50).toFloat()
            )
            EntityManager.createBall(
                engine, world,
                ballProperties,
                graphicsAtlas.ball
            )
        }

        EntityManager.createWorldLimits(engine, world, 1000f, 1000f)
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        engine.update(delta)
        AudioLocator.service.update()
    }

    override fun dispose() {
        super.dispose()
        world.dispose()
    }

}
