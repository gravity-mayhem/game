package com.adrubioco.gravitymayhem.screens

import com.adrubioco.gravitymayhem.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.viewport.Viewport
import ktx.app.KtxScreen

abstract class GravityMayhemScreen(
    val game: Game,
    val gameViewport: Viewport = game.gameViewport,
    val uiViewport: Viewport = game.uiViewport,
) : KtxScreen {
    override fun resize(width: Int, height: Int) {
        gameViewport.update(width, height, true)
        uiViewport.update(width, height, false)
    }
}