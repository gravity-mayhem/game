package com.adrubioco.gravitymayhem.screens

import com.adrubioco.gravitymayhem.DO_DEBUG
import com.adrubioco.gravitymayhem.Game
import com.adrubioco.gravitymayhem.asset.SoundAsset
import com.adrubioco.gravitymayhem.asset.BackgroundAsset
import com.adrubioco.gravitymayhem.asset.TextureAtlasAsset
import com.adrubioco.gravitymayhem.audio.AudioLocator
import com.adrubioco.gravitymayhem.audio.DebugAudio
import com.adrubioco.gravitymayhem.audio.DefaultAudio
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import ktx.app.KtxScreen
import ktx.assets.async.AssetStorage
import ktx.async.KtxAsync
import ktx.collections.gdxArrayOf

class LoadingScreen(
    private val game: Game
) : KtxScreen {
    override fun show() {
        KtxAsync.launch {
            initAudioLocator(game.assets)
            loadAllAssets(game.assets)
            transitionToGamePlayScreen()
        }
    }

    private fun transitionToGamePlayScreen() {
        game.addScreen(GamePlayScreen(game))
        game.setScreen<GamePlayScreen>()
        game.removeScreen<LoadingScreen>()
        dispose()
    }

    /**
     * Asynchronously loads all assets into an AssetStorage
     * @param manager the AssetStorage into which to load the assets
     */
    private suspend fun loadAllAssets(manager: AssetStorage) {
        gdxArrayOf(TextureAtlasAsset.values().map { manager.loadAsync(it.descriptor) },
            BackgroundAsset.values().map { manager.loadAsync(it.descriptor) },
            SoundAsset.values().map { manager.loadAsync(it.descriptor) }).flatten().joinAll()
    }

    private fun initAudioLocator(manager: AssetStorage) {
        AudioLocator.service = if (DO_DEBUG) DebugAudio(manager) else DefaultAudio(manager)
    }

}
