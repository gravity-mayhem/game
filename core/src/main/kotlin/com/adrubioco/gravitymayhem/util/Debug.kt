package com.adrubioco.gravitymayhem.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2

private fun drawDebugLine(
    shapeRenderer: ShapeRenderer,
    start: Vector2,
    end: Vector2,
    lineWidth: Float = 2f,
    color: Color = Color.WHITE,
) {
    Gdx.gl.glLineWidth(lineWidth)
    Gdx.gl.glLineWidth(1f)
    shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
    shapeRenderer.color = color
    shapeRenderer.line(start, end)
    shapeRenderer.end()
}
