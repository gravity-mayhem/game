package com.adrubioco.gravitymayhem.util

import com.adrubioco.gravitymayhem.box2d.Material
import com.badlogic.gdx.math.Vector2
import ktx.math.vec2

data class PhysicalProps(
    val size: Vector2 = vec2(1f, 1f),
    var density: Float = 1f,
    var material: Material = Material.ETHER,
    val pos: Vector2 = vec2(),
    var angle: Float = 0f,
    val velocity: Vector2 = vec2()
)