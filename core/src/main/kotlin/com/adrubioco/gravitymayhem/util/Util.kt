package com.adrubioco.gravitymayhem.util

import com.adrubioco.gravitymayhem.ecs.components.CameraFocusComponent
import com.adrubioco.gravitymayhem.ecs.components.CommandComponent
import com.adrubioco.gravitymayhem.ecs.components.HumanPlayerComponent
import com.adrubioco.gravitymayhem.ecs.components.ThrustComponent
import com.badlogic.ashley.core.Entity

fun swapControl(entity1: Entity, entity2: Entity) {
    with(entity1) {
        remove(HumanPlayerComponent::class.java)
        remove(CommandComponent::class.java)
        remove(ThrustComponent::class.java)
        remove(CameraFocusComponent::class.java)
    }
    with(entity2) {
        add(HumanPlayerComponent())
        add(CommandComponent())
        add(ThrustComponent())
        add(CameraFocusComponent())
    }
}