package com.adrubioco.gravitymayhem.shared.box2d

import com.adrubioco.gravitymayhem.shared.math.TAU
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import ktx.math.vec2

/** Gets the body's angle as a normalized vector. */
fun Body.vectorAngle(): Vector2 {
    return vec2(1f, 0f).setAngleRad(this.angle)
}

const val Y_UP = 0.25f * TAU.toFloat()
