package com.adrubioco.gravitymayhem.shared.math

import kotlin.math.PI

const val TAU: Float = 2f * PI.toFloat()
const val circle = TAU.toFloat()
const val semicircle = circle * 0.5