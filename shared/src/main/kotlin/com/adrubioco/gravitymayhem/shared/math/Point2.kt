package com.adrubioco.gravitymayhem.shared.math

import com.badlogic.gdx.math.Vector2

/**
 * Identical to the Vector2 class.
 * Semantically useful to denote it's usage as a coordinate position
 * instead of as vector value.
 */
class Point2(x: Float = 0f, y: Float = 0f): Vector2()