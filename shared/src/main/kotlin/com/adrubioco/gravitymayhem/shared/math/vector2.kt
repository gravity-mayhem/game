package com.adrubioco.gravitymayhem.shared.math

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import ktx.math.vec2
import ktx.math.vec3

/**
 * Constructs a new [Vector2] instance. An equivalent of [Vector2] constructor that supports Kotlin syntax features:
 * named parameters with default values.
 * @param pointA the initial point.
 * @param pointB the end point
 * @return a new [Vector2] instance storing the passed values.
 */
fun vec2(pointA: Vector2, pointB: Vector2): Vector2 {
    return vec2(
        pointB.x - pointA.x,
        pointB.y - pointA.y
    )
}

fun Vector2.vec3(): Vector3 {
    return vec3(this.x, this.y, 0f)
}