package com.adrubioco.gravitymayhem.shared.math

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import ktx.math.vec2

fun Vector3.set(x: Float = this.x, y: Float = this.y, z: Float = this.z) {
    this.x = x
    this.y = y
}

/**
 * Creates the same vector in 2 dimensions.
 * CAUTION: The Z coordinate is lost.
 */
fun Vector3.vec2(): Vector2 {
    return vec2(this.x, this.y)
}

fun Vector3.set(xAndY: Vector2, z: Float = this.z): Vector3 {
    return this.set(xAndY, z)
}