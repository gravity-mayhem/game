package com.adrubioco.gravitymayhem.shared.physics

import kotlin.math.sign

/**
 *  Calculates the Distance needed to reach 0 units/s
 *  given an acceleration and an initial velocity.
 *
 *  @param velocity Starting angular velocity
 *  @param acceleration Negative acceleration used to come to a stop
 *
 * @return Distance needed to reach 0 rad/s
 */
fun minimumBrakingDistance(velocity: Float, acceleration: Float): Float {
    require(velocity.sign != acceleration.sign) {
        """Acceleration and velocity 
            |passed as arguments 
            |must have opposite signs, 
            |or velocity won't reach 0.""".trimMargin()
    }

    val time = -velocity / acceleration
    return 0.5f * velocity * time
}